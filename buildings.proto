/*
This proto file defines the exchange of data between osm2gear and osm2city for data related to buildings.
 */
syntax = "proto3";

option optimize_for = SPEED;

package proto;

message Buildings {
  repeated Building buildings = 1;
  repeated Node nodes = 2;
  repeated BuildingParent parents = 3;
  repeated Zone zones = 4;
  repeated LitArea lit_areas = 5;
}


// vector/list of references to osm_ids
message refs_vec {
  repeated sint64 refs = 1;
}

// vector/list of list of references to osm_ids
message vec_refs_vec {
  repeated refs_vec vecs = 1;
}

message RoofHint {
  double ridge_orientation = 1;
  optional double inner_node_x = 2;
  optional double inner_node_y = 3;
  bool node_before_inner_is_shared = 4;
}

message Building {
  sint64 osm_id = 1;
  map<string, string> tags = 2;
  refs_vec outer_refs = 3;
  vec_refs_vec inner_refs = 4;
  sint64 zone_id = 5;
  bool has_neighbours = 6;
  optional RoofHint roof_hint = 7;
}

message Node {
  sint64 osm_id = 1;
  double lon = 2; // C++ is double, if using float (due to Python) then precision not good enough
  double lat = 3;
}

message BuildingParent {
  sint64 osm_id = 1;
  map<string, string> tags = 2;
  bool simple_3d = 3;
  repeated sint64 children = 4;
}

/* Cf. https://en.wikipedia.org/wiki/Well-known_text_representation_of_geometry
 * https://shapely.readthedocs.io/en/stable/manual.html#well-known-formats
 */
message WellKnownText {
  string wkt = 1;
}

message WellKnownBinary {
  bytes wkb = 1;
}

message Zone {
  sint64 osm_id = 1;
  uint32 building_zone_type = 2;
  uint32 settlement_type = 3;
  WellKnownBinary geometry = 4;
}

message LitArea {
  WellKnownBinary geometry = 1;
}
