#===============
# Postgres stuff
#===============
# On Ubuntu make sure to have the following packages installed:
# * postgresql-server-dev-all
# * libpq-dev
# * libpqxx-6.4
# * libpqxx-dev

# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -lpqxx -lpq")
find_library(PQXX_LIB pqxx)
find_library(PQ_LIB pq)

# A] DOES NOT WORK
# https://github.com/sun1211/libpqxx-with-cmake-example/blob/master/libpq-with-build/README.md
#FetchContent_Declare(
#        libpqxx
#        GIT_REPOSITORY https://github.com/jtv/libpqxx.git
#        GIT_TAG d631e84b2f0ecad3f0d9237168ad6100abfbd5b9 # 6.4.8
#)
# set(PQXX_LIBRARIES pqxx_static)
# FetchContent_MakeAvailable(libpqxx)

#========
# OpenGL (for OSG or SimGear)
#========
find_package(OpenGL REQUIRED) # https://cmake.org/cmake/help/latest/module/FindOpenGL.html
cmake_print_variables(OPENGL_LIBRARIES OPENGL_gl_LIBRARY)
find_package(GLUT REQUIRED)
cmake_print_variables(GLUT_INCLUDE_DIR GLUT_glut_LIBRARY)

#========
# SimGear
#========
message(STATUS "FG_INSTALL : $ENV{FG_INSTALL}")

find_library(SIMGEAR_CORE_LIB NAMES SimGearCore HINTS "$ENV{FG_INSTALL}/simgear/lib" REQUIRED)
find_library(SIMGEAR_SCENE_LIB NAMES SimGearScene HINTS "$ENV{FG_INSTALL}/simgear/lib" REQUIRED)
find_path(SIMGEAR_SCENE_INCLUDE_DIR simgear/version.h HINTS "$ENV{FG_INSTALL}/simgear/include" REQUIRED)

cmake_print_variables(SIMGEAR_CORE_LIB SIMGEAR_SCENE_LIB SIMGEAR_SCENE_INCLUDE_DIR)

#========
# OSG (included in FG)
#========
# to find the OS package one could use https://cmake.org/cmake/help/latest/module/FindOpenSceneGraph.html
find_library(OSG_CORE_LIB NAMES osg HINTS "$ENV{FG_INSTALL}/openscenegraph/lib" REQUIRED)
find_library(OSG_DB_LIB NAMES osgDB HINTS "$ENV{FG_INSTALL}/openscenegraph/lib" REQUIRED)
find_library(OSG_OT_LIB NAMES OpenThreads HINTS "$ENV{FG_INSTALL}/openscenegraph/lib" REQUIRED)
find_path(OSG_INCLUDE_DIR osg/Version HINTS  "$ENV{FG_INSTALL}/openscenegraph/include" REQUIRED)

cmake_print_variables(OSG_CORE_LIB OSG_DB_LIB OSG_OT_LIB OSG_INCLUDE_DIR)


#=========
# Protobuf
#=========
# Use the package from Ubuntu:
# * protobuf-compiler

find_package(Protobuf REQUIRED)
if(protobuf_VERBOSE)
    message(STATUS "Using Protocol Buffers ${protobuf_VERSION}")
endif()
cmake_print_variables(PROTOBUF_LIBRARY PROTOBUF_INCLUDE_DIR)

#======
# Boost
#======
# Requires boost libraries installed on the system. On Ubuntu: sudo apt-get install libboost-all-dev
set(Boost_USE_STATIC_LIBS ON)
# log_setup is needed due to https://github.com/boostorg/log/issues/46
# see also https://gitlab.kitware.com/cmake/cmake/-/issues/20638
# Ubuntu 23.04 is on 1.74
find_package(Boost 1.74 REQUIRED COMPONENTS iostreams log_setup log graph program_options)


# fetched with vcpkg
find_package(ZLIB REQUIRED)
if (ZLIB_FOUND)
    message(STATUS "Found zlib: ${ZLIB_CONFIG} (found version ${ZLIB_VERSION})")
endif ()

find_package(cpr CONFIG REQUIRED)
if (cpr_FOUND)
    message(STATUS "Found cpr: ${cpr_CONFIG} (found version ${cpr_VERSION})")
endif ()

find_package(geos CONFIG REQUIRED)
if (geos_FOUND)
    message(STATUS "Found geos: ${geos_CONFIG} (found version ${geos_VERSION})")
endif ()

# use operating system from Ubuntu, which is quite recent: install proj-bin and libproj-dev
# advantage over using the one from vcpkg is that no setting of PROJ_LIB is needed to find the proj.db at runtime.
find_package(PROJ CONFIG REQUIRED)
if (PROJ_FOUND)
    message(STATUS "Found PROJ: ${PROJ_CONFIG} (found version ${PROJ_VERSION})")
endif ()


#========================= LIBRARY ===========================================

add_library(Core STATIC)
target_sources(Core PUBLIC
        core/aptdat_io.h core/aptdat_io.cpp
        core/buildings.h core/buildings.cpp
        core/elev_probe.h core/elev_probe.cpp
        core/enumerations.h
        core/json_io.h core/json_io.cpp
        core/landuse_processor.h core/landuse_processor.cpp
        core/models.h core/models.cpp
        core/parameters.h
        core/plotting.h core/plotting.cpp
        core/stg_io.h core/stg_io.cpp
        core/roofs.h
        core/tile_handler.h core/tile_handler.cpp
        core/trees.h core/trees.cpp
        core/utils.h core/utils.cpp
        core/would_be_buildings.h core/would_be_buildings.cpp

        osm/constants.h
        osm/data_reader.cpp osm/data_reader.h
        osm/osm_types.h osm/osm_types.cpp

        proto/buildings.pb.h proto/buildings.pb.cc
        proto/proto_writer.h proto/proto_writer.cpp
)

target_link_libraries(Core PRIVATE ${PQXX_LIB} ${PQ_LIB} "${SIMGEAR_CORE_LIB}" "${SIMGEAR_SCENE_LIB}" "${OSG_CORE_LIB}" "${OSG_DB_LIB}" "${OSG_OT_LIB}" "${Boost_LIBRARIES}" ${ZLIB_LIBRARIES} ${PROTOBUF_LIBRARY})
target_link_libraries(Core PRIVATE cpr::cpr)
target_link_libraries(Core PRIVATE GEOS::geos)
target_link_libraries(Core PRIVATE PROJ::proj)
target_include_directories(Core PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}" "${SIMGEAR_SCENE_INCLUDE_DIR}" "${OSG_INCLUDE_DIR}" ${ZLIB_INCLUDE_DIRS} ${PROTOBUF_INCLUDE_DIR} ${Boost_INCLUDE_DIRS})

#========================= EXECUTABLE ========================================

add_executable(MainCLI main.cpp)

# Link our executable against the library
target_link_libraries(MainCLI PRIVATE Core)
