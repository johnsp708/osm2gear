// SPDX-FileCopyrightText: (C) 2023 - 2024, rick@vanosten.net
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <map>

#include <boost/process.hpp>

#include "tile_handler.h"


/*
 * Using boost::process. Alternatives would be:
 *     https://github.com/arun11299/cpp-subprocess
 *     https://doc.qt.io/qt-5/qprocess.html#details
 *     https://stackoverflow.com/questions/39693924/c-linux-interact-another-program-stdin-stdout
 * Modelled after FGElev in osm2city:
 *     https://gitlab.com/osm2city/osm2city/-/blob/ws3/osm2city/utils/utilities.py
 * Supports only WS3.0
 * See also https://sourceforge.net/p/flightgear/codetickets/2657/
 */

//Used to indicate that FGElev did not return a reliable result
constexpr int FG_ELEV_NO_ELEV = -9999;

// fgelev result: Found hole of minimum diameter 1.31072m at lon = 5.42631deg lat = 52.4969deg
// 1: -1000
// NB: the default is set in FGElev / SimGear - not here
constexpr char FG_ELEV_NOT_FOUND[] = "-1000";

constexpr char FG_ELEV_EXPIRE[] = "1000";

class ElevationProbingException : public std::exception {
   private:
    std::string message_;

   public:
    explicit ElevationProbingException(std::string message);
    [[nodiscard]] const char* what() const noexcept override {
        return message_.c_str();
    }
};

class FgElevInstance {
   private:
    std::shared_ptr<boost::process::ipstream> from_fg_elev_output_;
    std::shared_ptr<boost::process::opstream> to_fg_elev_input_;
    std::unique_ptr<boost::process::child> fg_elev_;
    u_long records_ {0};
   public:
    explicit FgElevInstance(const LonLat &lon_lat);
    float QueryElevation(const LonLat &lon_lat);
    ~FgElevInstance();
};


class ElevationProber {
   private:
    std::map<std::string, std::unique_ptr<FgElevInstance>> fg_elev_instances_ {};
   public:
    ElevationProber() = default;
    float Probe(const LonLat &lon_lat, bool is_global);
};
