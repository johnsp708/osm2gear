// SPDX-FileCopyrightText: (C) 2022 - 2024, rick@vanosten.net
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <optional>
#include <string>

/*! \brief Queries WikiData for population data of a given entity.
 *
 * @param entity_id from OSM tag "wikidata"
 * @return the population if everything went fine and the found value is > 0, otherwise std::nullopt
 */
std::optional<int> QueryPopulationWikidata(const std::string &entity_id);
