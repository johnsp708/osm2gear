// SPDX-FileCopyrightText: (C) 2021 - 2024, rick@vanosten.net
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <map>
#include <vector>

#include "osm_types.h"
#include "../core/utils.h"

class OSMDBDataReader {
private:
    const std::string db_connection_string_;
    const Boundary boundary_;
    const Boundary extended_boundary_;
    bool use_extended_boundary_ = false;

    [[nodiscard]] std::string ConstructIntersectBBoxQuery(bool) const;
    [[nodiscard]] OSMReadResult FetchWays(const std::vector<std::string> &required, bool is_key_values) const;
    [[nodiscard]] OSMReadResult FetchNodesIsolated(const std::vector<std::string> &required, bool is_key_values) const;
    [[nodiscard]] node_map_t FetchNodesForWay(const std::vector<std::string>& required_keys,
                                              const std::vector<std::string>& required_key_values) const;
    [[nodiscard]] way_map_t FetchWayData(const std::vector<std::string>& required_keys,
                                         const std::vector<std::string>& required_key_values) const;

    [[nodiscard]] OSMReadRelationsResult FetchOSMRelationsKeys(const std::string &first_part, const std::string &relation_debug_string,
                                                               bool handle_empty_role_as_outer) const;

public:
    OSMDBDataReader(std::string, Boundary, Boundary);
    [[nodiscard]] OSMReadResult FetchWaysFromKeyValues(const std::vector<std::string> &) const;
    [[nodiscard]] OSMReadResult FetchWaysFromKeys(const std::vector<std::string> &) const;

    [[nodiscard]] OSMReadResult FetchNodesIsolatedFromKeyValues(const std::vector<std::string> &) const;
    [[nodiscard]] OSMReadResult FetchNodesIsolatedFromKeys(const std::vector<std::string> &) const;

    [[nodiscard]] OSMReadRelationsResult FetchRelationsPlaces() const;

    [[nodiscard]] OSMReadRelationsResult FetchRelationsAerodrome() const;

    [[nodiscard]] OSMReadRelationsResult FetchRelationsLanduse() const;

    [[nodiscard]] OSMReadRelationsResult FetchRelationsBuildings() const;

    [[nodiscard]] OSMReadRelationsResult FetchRelationsRiverbanks() const;

    [[nodiscard]] OSMReadRelationsResult FetchRelationsNaturalWater() const;

    void SetUseExtendedBoundary(bool);

    static std::string CreateKeyValuePair(const std::string &key, const std::string &value);
};
