// SPDX-FileCopyrightText: (C) 2023 - 2024, rick@vanosten.net
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include "buildings.pb.h"
#include "../core/buildings.h"

/*! \brief Write messages for building information to a proto file, such that the info can be read by Python
 *
 * Throws runtime_error if the file cannot be written
 */
void WriteBuildingStuffToProtobuf(const std::map<long, std::unique_ptr<Node>> &building_nodes,
                                  const std::vector<std::shared_ptr<Building>> &,
                                  const std::vector<std::unique_ptr<geos::geom::Geometry>> &);
